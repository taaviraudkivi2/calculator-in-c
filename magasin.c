#include<stdio.h>
#include<stdlib.h>

#define MAGASINI_PIKKUS 100

double magasin[MAGASINI_PIKKUS];
int elementide_arv = 0;

/** Magasini funktsioonid. */
void push(double v) {
    if (elementide_arv >= MAGASINI_PIKKUS) {
        printf("VIGA! Magasini أ¼letأ¤itumine\n");
        exit(2);
    }
    magasin[elementide_arv] = v;
    ++elementide_arv;
}

double pop() {
    if (elementide_arv <= 0) {
        printf("VIGA! Magasini alatأ¤itumine\n");
        exit(1);
    }
    --elementide_arv;
    return magasin[elementide_arv];
}

int empty() {
    return elementide_arv <= 0;
}

void tryki_magasin() {
    int i;
    for (i = elementide_arv - 1; i >= 0; --i) {
        printf("m[%d] = %f\n", i, magasin[i]);
    }
}


