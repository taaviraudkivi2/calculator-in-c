#include<stdio.h>
#include<stdlib.h>

// Jأ¤rgmine rida asendatakse magasin.c sisuga
#include "magasin.h"

/**
 *1 + 2 -> 1 2 +
 * 1 + 2 * 3 -> 1 2 3 * +
 *           -> 2 3 * 1 +
 */


#define TEHE2(nimi, tehe) \
void nimi() { \
	double a, b, summa; \
	b = pop(); \
	a = pop(); \
	summa = a tehe b; \
	push(summa); \
}

TEHE2(liida2, +);
TEHE2(korruta2, *);
TEHE2(lahuta2, -);
TEHE2(jaga2, /);

void arvuta(char avaldis[]) {
    int i = 0;
    while(avaldis[i] != 0) {
        char c = avaldis[i];
        ++i;

        /* Kas tegemist on numbriga? Vt. ASCII tabel. */
        if (c >= '0' && c <= '9') {
            push(c - '0');

            /* Lأ¤hme tأ¶أ¶tleme jأ¤rgmist sأ¼mbolit. */
            continue;
        }

        /* Kindlasti pole arv. */
        switch(c) {
            case '+':
                liida2();
                break;
            case '*':
                korruta2();
                break;
            case '-':
                lahuta2();
                break;
            case '/':
                jaga2();
                break;
            default:
                printf("VIGA! Tundmatu sأ¼mbol\n");
                exit(3);
        }
    }

}

int main() {

    arvuta("133-/");

    tryki_magasin();
}

